# Strategem



---

This is a small strategy game made mostly in C, but compiled as C++. It renders to the console, and works on *nix and and windows platforms. This was my first semi-big project in C, so the code is rather crude.



I wanted this to incorporate what I like in a strategy game, so it includes resource gathering, buildings with different build requirements and AI that aquires a resource of the correct type and returns to its building. It also contains a small market sub-menu and some other small features. On windows it should give you colors in the terminal. On *nix this has been disabled. If you're a *nix programmer you know why.



I did not add a winning condition, so the game will just continue until you run out of resources to harvest. 



Good luck.

---

Here's a screenshot from the start of the game:


```
##########################################################################################
[###] [Settler]:                   Wood  0              Stone 4              Iron  4 [###]
[###]                                                                                [###]
##########################################################################################
[###]b . . . . . . . . . . . . . . . . . . . . . . . . . . b b . . . . . . . . . . . [###]
[###]a a . . . . . . . b b . . . . . a a a a . . c c . . . b b . . . . . . . . . . . [###]
[###]a a a a . . . . . b b . . . . . a a a a . . c c . . . . b b a a . . . . . . . . [###]
[###]a a a a . . . . . . . . . . . . . . a a . . . a a . . c b b a a . . b b . . . . [###]
[###]a a . . . . . . . . . . . . . . . . . . . . . a a . . c c . . . . . b b . . . . [###]
[###]. . . . . . . . . . . . . . . . . . . b b . . . . . . . . . . . . . . . . . . . [###]
[###]. . . . . . . . . . . . . . . . . . . b b . . . . . . . . . . . . . . . . . . . [###]
[###]. . . . . . . . . . . c c c . . . . . . . . . . . . . . . . . . . . . . . . . . [###]
[###]. . . . . . . . . . . c c c . . . . . . . . . . . . b b . . . . . . . . . b b . [###]
[###]. . . . . . . . . . . . . . . . . . . . . . . . . . b b . . . . . . . c c b b . [###]
[###]. . . . . . . . . . . . . . . . . . . * . . . . . . . . . . . . . . . c c . . . [###]
[###]. a a . . . . . b b c . . . . . . . . . . . . . . . . a a . . . . . . . . . . . [###]
[###]. a a . . . . . b b c . . . . . . . . . . . . . . . . a a . . . . . . . . . . . [###]
[###]. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . b b . . . . . . . . [###]
[###]. . . . . . . . . c c . . . . . . . . . . . . b b b . . . . b b a a . . . . . . [###]
[###]. . . . . . . b b c c . . . . . . . . . . . . b b b . . . . . . a a . . . . . c [###]
[###]. . . . . . . b b . . . . . . . . . . . . . . . . . . . . b b . . . . . . . . . [###]
[###]. . . . . c c . . . . . . . . . . . . . . . . . . . . . . b b . . . . . . . . . [###]
[###]. . . . . c c . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . [###]
[###]. . . . . . a . . . . . . . . . . . . . . . . . . . . . . . . . b . . . . . . . [###]
##########################################################################################
[###] We need a place to sleep tonight. Where are we going to sleep.. help..         [###]
[###]                                                                                [###]
##########################################################################################
```

---

Released with the [Unlicense](http://unlicense.org/), so there are no limitations. However, please drop me a line if you end up using or referencing my code. That makes me happy.



Sincerely, 



PhossK