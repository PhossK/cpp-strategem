#ifndef STRATEGEM_TYPES
#define STRATEGEM_TYPES
#pragma once

struct Coordinates
{
  int Y;
  int X;
  void Init( int tmpY, int tmpX )
  {
    Y = tmpY;
    X = tmpX;
  };
  Coordinates()
    : Y(0), X(0) {};
};

struct Inventory
{
  int ResourceA;
  int ResourceB;
  int ResourceC;
  void operator -( Inventory &INPUT )
  {
    ResourceA -= INPUT.ResourceA;
    ResourceB -= INPUT.ResourceB;
    ResourceC -= INPUT.ResourceC;
  };
  void operator +( Inventory &INPUT )
  {
    ResourceA += INPUT.ResourceA;
    ResourceB += INPUT.ResourceB;
    ResourceC += INPUT.ResourceC;
  };
  
  void Init( int A, int B, int C )
  {
    ResourceA = A;
    ResourceB = B;
    ResourceC = C;
  }
  ;
  Inventory()
    : ResourceA(0), ResourceB(0), ResourceC(0) {};
};

#endif
