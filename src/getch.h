#ifndef STRATEGEM_GETCH
#define STRATEGEM_GETCH
#pragma once

#include <stdio.h>
#include <termios.h>
#include <unistd.h>

/*
 * http://bytes.com/topic/c/answers/503640-getch-linux
 * http://cboard.cprogramming.com/faq-board/27714-faq-there-getch-conio-equivalent-linux-unix.html
 * Author: kermi3
 */

int mygetch()
{
  struct
    termios oldt, newt;
  int ch;
  tcgetattr( STDIN_FILENO, &oldt );
  newt = oldt;
  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newt );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
  return ch;
}

#endif
