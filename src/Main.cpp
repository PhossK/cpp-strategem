#ifndef STRATEGEM_MAIN
#define STRATEGEM_MAIN
/*
 * ┌───────────────────────────────────────────────────────────────┐
 * │                                                               │
 * │  mmmm    m                    m                               │
 * │ #"   " mm#mm   m mm   mmm   mm#mm   mmm    mmmm   mmm   mmmmm │
 * │ "#mmm    #     #"  " "   #    #    #"  #  #" "#  #"  #  # # # │
 * │     "#   #     #     m"""#    #    #""""  #   #  #""""  # # # │
 * │ "mmm#"   "mm   #     "mm"#    "mm  "#mm"  "#m"#  "#mm"  # # # │
 * │                                            m  #               │
 * │                                             ""                │
 * └───────────────────────────────────────────────────────────────┘
 */

#ifdef _MSC_VER
#include <Windows.h>
#include <conio.h>
#define SYS_clear "cls"
#define SYS_input _getch
void usleep(int microseconds){ ::Sleep(microseconds / 1000); }
#else
#include <unistd.h>
#include "getch.h"
#define SYS_clear "clear"
#define SYS_input mygetch
#endif

#define GridSize_Y 20
#define GridSize_X 40
#define GridPad_C true

#define BIT_00 0x0000
#define BIT_01 0x0001
#define BIT_02 0x0002
#define BIT_03 0x0004
#define BIT_04 0x0008
#define BIT_05 0x0010
#define BIT_06 0x0020
#define BIT_07 0x0040
#define BIT_08 0x0080
#define BIT_09 0x0100
#define BIT_10 0x0200
#define BIT_11 0x0400
#define BIT_12 0x0800
#define BIT_13 0x1000
#define BIT_14 0x2000
#define BIT_15 0x4000
#define BIT_16 0x8000

unsigned char BIT_GAME_Idle     = BIT_00;
unsigned char BIT_GAME_Init     = BIT_01;
unsigned char BIT_GAME_Active   = BIT_02;
unsigned char BIT_GAME_Reserved = BIT_03;
unsigned int BIT_Empty     = BIT_00;
unsigned int BIT_Player    = BIT_01;
unsigned int BIT_Worker    = BIT_02;
unsigned int BIT_TypeA     = BIT_03;
unsigned int BIT_TypeB     = BIT_04;
unsigned int BIT_TypeC     = BIT_05;
unsigned int BIT_Base      = BIT_06;
unsigned int BIT_Upgrade1  = BIT_07;
unsigned int BIT_Upgrade2  = BIT_08;
unsigned int BIT_Upgrade3  = BIT_09;
unsigned int BIT_Upgrade4  = BIT_10;
unsigned int BIT_Resource  = BIT_11;
unsigned int BIT_Building  = BIT_12;
unsigned int BIT_AI_Plant  = BIT_13;
unsigned int BIT_AI_Collect= BIT_14;


/*
 * #include <>
 * #include ""
 */
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <time.h>
#include "Types.hpp"
#include "AI.hpp"
#include "Building.hpp"
#include "Cell.hpp"
#include "Grid.hpp"
#include "Player.hpp"
#include "Controller.hpp"

void GAME ( unsigned char &gameState )
{ /* 
   * Main loop active for as long as application is
   * running. Construct elements for our session and
   * sets up the environment for gameplay.
   * Perform a bitshift and descend into the main
   * game loop for the current session.
   */

  while ( gameState & BIT_GAME_Init )
    {
      Grid activeGrid;
      Player activePlayer;
      Controller term( activeGrid, activePlayer, gameState );
      
      term.Init();
      while ( gameState & BIT_GAME_Active )
	{
	  term.Output();
	  term.Input();
	  term.Sync();
	  while ( gameState & BIT_GAME_Reserved )
	    {
	      term.Market();
	    }
	}
      break;
    }
};

int main ()
{ /* 
   * The only thing happening here is construction of
   * our gameState counter. This is so that the game
   * can be run and re-run multiple times in a row
   * without worry. This seemed like the best way
   * of solving this.
   */
	

  unsigned char gameState = BIT_GAME_Init;

  GAME ( gameState );

  return 0;
};

#endif
