#ifndef STRATEGEM_GRID
#define STRATEGEM_GRID
#pragma once

struct Grid
{
  /*
   * Datacontainer for the board.
   */		
private:
  int GridPad_Y;
  int GridPad_X;

  Cell MainGrid [ GridSize_Y ][ GridSize_X ];

public:
  Cell &Output( int PosY, int PosX )
  {
    return MainGrid [PosY][PosX];
  };
  Cell &Output2( Coordinates Pos )
  {
    return MainGrid [Pos.Y][Pos.X];
  };
  int PadY()
  {
   return GridPad_Y;
  };
  int PadX()
  {
    return GridPad_X;
  };
  Grid()
  {
    GridPad_X = 3;
    GridPad_Y = GridSize_X + (GridSize_X * GridPad_C) + ((GridPad_X + 2) * 2);
  };
  ~Grid()
  {
    //
  };
};

#endif


