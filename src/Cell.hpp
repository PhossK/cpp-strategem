#ifndef STRATEGEM_CELL
#define STRATEGEM_CELL
#pragma once


struct Cell
{ /* Datacontainer for cell. */  
private:
  unsigned int Value;
  Building *currentBuilding;

public:
  unsigned int &Output()
  {
    return Value;	
  };
  unsigned int bit_ON( unsigned int BITFLAG )
  {
    Value |= (BITFLAG);
    return Value;
  };
  unsigned int bit_OFF( unsigned int BITFLAG )
  {
    Value &= (~BITFLAG);
    return Value;
  };
  unsigned int bitField_OFF()
  {
    Value = BIT_Empty;
    return Value;
  };
  void getBuilding( Building *building )
  {
   currentBuilding = building;
  };
  Cell()
  {
    bitField_OFF();
    currentBuilding = nullptr;
  };
  ~Cell()
  {
    //	
  };
};

#endif
