#ifndef STRATEGEM_CONTROLLER
#define STRATEGEM_CONTROLLER
#pragma once

struct Controller
{
private:
  char INPUT;
  Grid *ptr_activeGrid;
  Player *ptr_activePlayer;
  unsigned char *ptr_gameState;
  
  const char *MessageBuffer;
  const char *StatsBuffer;
  const char *MenuBuffer;
  Inventory MerchantInv;

  // HANDLE Win32console;
  
  std::string PlayerMoveString;
  std::string PlayerBuildString;
  std::string PlayerUpgradeString;
  std::vector<Building*> Buildings;
  
  int BufferWidth;
  int BufferW_Quad;
  int Grid_Pad_X;
  int Grid_Pad_Y;
  int Grid_Rand;
  int GameRound;

  int GameRoundIncrement()
  {
    return GameRound ++;
  };
  void ClearConsole()
  {
    std::system( SYS_clear );
  };
  void GameStateIncrement()
  {
    *ptr_gameState <<=1;
  };
  void GameStateDecrement()
  {
    *ptr_gameState >>=1;
  };
  const char *BufferMessage( const char *str )
  {
    return (MessageBuffer = str);
  };
  std::string str( int integer, char symbol )
  {
    return std::string( integer, symbol );
  };
  std::string strStat( std::string tmpString, int tmpInt )
  {
    return std::string( tmpString + std::to_string(tmpInt));
  };
  void PrintMerchantBuffer()
  {
    LoopPreX();
    printf("%-*s ", BufferW_Quad - 4, " [Merchant]:");
    printf("%*s ",  BufferW_Quad, strStat( "Wood  ", MerchantInv.ResourceA).c_str() );
    printf("%*s ",  BufferW_Quad, strStat( "Stone ", MerchantInv.ResourceB).c_str() );
    printf("%*s ",  BufferW_Quad, strStat( "Iron  ", MerchantInv.ResourceC).c_str() );
    LoopPosX();
  };
  void PrintPlayerStatsBuffer()
  {
    LoopPreY();
    LoopPreX();
    printf("%-*s ", BufferW_Quad - 4, " [Settler]:");
    printf("%*s ",  BufferW_Quad, strStat( "Wood  ", ptr_activePlayer->getInv().ResourceA).c_str() );
    printf("%*s ",  BufferW_Quad, strStat( "Stone ", ptr_activePlayer->getInv().ResourceB).c_str() );
    printf("%*s ",  BufferW_Quad, strStat( "Iron  ", ptr_activePlayer->getInv().ResourceC).c_str() );
    LoopPosX();
  };
  void PrintBaseStatsBuffer()
  {
    LoopPreX();
    if ( Buildings.size() >= 1 ) {
 	printf("%-*s ", BufferW_Quad - 4, " [Base]:");
  	printf("%*s ",  BufferW_Quad, strStat( "Wood  ", Buildings[0]->Inv.ResourceA ).c_str() );
	printf("%*s ",  BufferW_Quad, strStat( "Stone ", Buildings[0]->Inv.ResourceB ).c_str() );
  	printf("%*s ",  BufferW_Quad, strStat( "Iron  ", Buildings[0]->Inv.ResourceC ).c_str() ); }
    else
      {	printf("%*s",  BufferWidth, ""); }
    LoopPosX();
  };
  void PrintMessageBuffer()
  {
    LoopPreX();
    printf(" %-*s", BufferWidth-1, MessageBuffer);
    LoopPosX();
  };
  void PrintMenuBuffer()
  {
    LoopPreX();
    printf(" %-*s", BufferWidth-1, MenuBuffer);
    LoopPosX();
    LoopPosY();
  };
  char GetInput( char &INPUT )
  { /* Capture the Input in a variable. I can query the current 
     * input or the last input recorded by calling the function or the variable. */
    INPUT = SYS_input();
    return INPUT;
  };
  void EvalInput( char &INPUT )
  { /* Check the input against different "modes", or types of
     * input. Currently active is MoveInput and BuildInput.
     * ResourcePickup has been disabled as a keyboard input
     * for now as it makes gameplay tedious and slow.
     * If input while player is at base. It will trigger an upgrade menu, */
    if ( PlayerMoveString.find(INPUT) != std::string::npos )
      {	ptr_activePlayer->PlayerMovePrompt( INPUT ); }
    if ( PlayerBuildString.find(INPUT) != std::string::npos )
      {	PlayerBuildAction( INPUT ); }
    if ( PlayerUpgradeString.find(INPUT) != std::string::npos )
      {	PlayerUpgradeBase( INPUT ); }    

  };
  void PlayerUpgradeBase( char &INPUT )
  {
    Coordinates tmpPos  = ptr_activePlayer->getCurPos();
    if ( ptr_activeGrid->Output2( tmpPos ).Output() & ( BIT_Base ))
      {	BufferMessage("Upgrades: [4] Market (2, 2, 2) ");
	if ( INPUT == '4' )    
	  { /* Build Market to the right of the  base, if player meets requirements. */
	    tmpPos.X ++;
	    if ( CheckSlot( tmpPos ))
	      {
		if ( CheckPlayerWallet( BIT_Upgrade1 ))
		  { ptr_activeGrid->Output2( tmpPos ).bit_ON ( BIT_Upgrade1 ); }
		else { BufferMessage("[4] CANNOT AFFORD"); }
	      }
	    else { BufferMessage("[4] 404. Site not found yo."); } }	
      }
  };  
  void PlayerBuildAction( char &INPUT )
  { /* We wait for building dialog input. If input, we check whether
     * the player can afford the building. If so, output a message to
     * let the player know and construct the building. */
    if ( CheckSlot( ptr_activePlayer->getCurPos()) )
      { /* Check to see if there is already a building on the cell. */
	if ( Buildings.size() < 1 )
	  { /* Check if this is our first building. */
	    BufferMessage("[1] Base (5, 5, 5)");
	    if ( INPUT == '1' ) {
	      if ( CheckPlayerWallet ( BIT_Base )) {
		PlayerBuild( BIT_Base );
		BufferMessage("This is my base. There are many like it, but this one is mine. [HEART]"); }
	      else { BufferMessage("[1] CANNOT AFFORD"); } } }
	else
	  { /* The first building has already been placed.
	     * KNOWN BUG: The usage of 1, 2 or 3 will trigger PlayerBuild, even from other menus. */
	    BufferMessage( "[1] Forester (3, 2, 1), [2] Stonemason (1, 3, 2), [3] Mine (2, 1, 3)");
	    if ( INPUT == '1' ) {
	      if ( CheckPlayerWallet( BIT_Building | BIT_TypeA )) {
		PlayerBuild( BIT_Building | BIT_TypeA );
		BufferMessage("Forester Built!"); }
	      else { BufferMessage("[1] CANNOT AFFORD"); } }
	    if ( INPUT == '2' ) {
	      if ( CheckPlayerWallet( BIT_Building | BIT_TypeB )) {
		PlayerBuild( BIT_Building | BIT_TypeB );
		BufferMessage("Stonemasonry Built!"); }
	      else { BufferMessage("[2] CANNOT AFFORD"); } }
	    if ( INPUT == '3' ) {
	      if ( CheckPlayerWallet( BIT_Building | BIT_TypeC )) {
		PlayerBuild( BIT_Building | BIT_TypeC );
		BufferMessage("Mine Built!"); }
	      else { BufferMessage("[3] CANNOT AFFORD"); } }
	  }
      }
  };
  void PlayerBuild( unsigned int BIT )
  { /* Construct Building of type BIT, move to position of player, insert to
     * array of buildings, store off the pointer to it in the current Cell, 
     * and flip the Bit on the grid so it's visible. */
    Building* newBuild = new Building;
    newBuild->Type = BIT;
    newBuild->Pos = ptr_activePlayer->getCurPos();
    newBuild->Worker.InitAIPos( newBuild->Pos );
    Buildings.insert( Buildings.end(), newBuild );
    ptr_activeGrid->Output2( ptr_activePlayer->getCurPos() ).getBuilding( newBuild );
    ptr_activeGrid->Output2( ptr_activePlayer->getCurPos() ).bit_ON( BIT );
};
  bool CheckPlayerWallet( unsigned int INPUT )
  { /* Checks to see if the player can afford the chosen Base/Building/Upgrade. */
    Inventory tmpReq;
    
    if ( INPUT & BIT_Base )
      {
	tmpReq.Init(5, 5, 5);
	if ( ptr_activePlayer->CanAfford( tmpReq ))
	  { return true; }
      }
    if ( INPUT & BIT_Building )
      {
	if ( INPUT & BIT_TypeA ) {
	    tmpReq.Init(3, 2, 1);
	    if ( ptr_activePlayer->CanAfford( tmpReq ))
	      { return true; } }
	if ( INPUT & BIT_TypeB ) {
	    tmpReq.Init(1, 3, 2);
	    if ( ptr_activePlayer->CanAfford( tmpReq ))
	      { return true; } }
	if ( INPUT & BIT_TypeC ) {
	    tmpReq.Init(2, 1, 3);
	    if ( ptr_activePlayer->CanAfford( tmpReq ))
	      { return true; } }
      }
    if ( INPUT & BIT_Upgrade1 )
      {
	tmpReq.Init(2, 2, 2);
	if (ptr_activePlayer->CanAfford( tmpReq ))
	  { return true; }
      }
    return false;
  };
  bool CheckSlot( Coordinates POS )
  { /* Check to see whether or not a building can be placed at the cell.*/
    if ( ptr_activeGrid->Output2( POS ).Output() & ( BIT_Building | BIT_Base ))
      { BufferMessage( "Cannot build building on top of building. Logic." );
	return false; }
    return true;
  };
  void LoopPreY()
  { /* Padding for the TOP of the grid.*/
    printf("%s\n", str( Grid_Pad_Y, '#' ).c_str());
  };
  void LoopPreX()
  { /* Padding for the LEFT side of the grid. */
    printf("[%s]", str( Grid_Pad_X, '#' ).c_str());
  };
  void LoopMain( Cell &INPUT )
  { /* Main cell output with a padding to the right. */
    printf("%c", EvalCell( INPUT.Output() ) );
    printf("%s", CellPadding( GridPad_C ));
  };
  void LoopPosX()
  { /* Padding for the RIGHT side of the grid. */
    printf("[%s]\n", str( Grid_Pad_X, '#' ).c_str());
  };
  void LoopPosY()
  { /* Padding for the BOTTOM of the grid. */
    printf("%s\n", str( Grid_Pad_Y, '#' ).c_str());
  };
  const char *CellPadding( bool VALUE )
  { /* If padding is turned on globally, we output a space here. */
    if ( VALUE )
      {	return " "; }
    else
      { return "";  }
  };
  const char EvalCell( unsigned int &INPUT )
  { /* The flags are being interpreted and 
     * I return the corresponding ascii char for the cell.
     * Order of precedence: Buildings > Player/Worker > Resources */
    if( INPUT & BIT_Base )
      {	return '#'; }
    if ( INPUT & BIT_Upgrade1 )
      {	return 'M';  }
    if( INPUT & BIT_Building )
      {
	if ( INPUT & BIT_TypeA )
	  { return 'A'; }
	if ( INPUT & BIT_TypeB )
	  { return 'B';	}
	if ( INPUT & BIT_TypeC )
	  { return 'C'; }
      }
    if ( INPUT & BIT_Player )
      {	return '*';  }
    if ( INPUT & BIT_Worker )
      { return '+'; }
    if ( INPUT & BIT_Resource )
      {
	if ( INPUT & BIT_TypeA )
	  { return 'a'; }
	if ( INPUT & BIT_TypeB )
	  { return 'b'; }
	if ( INPUT & BIT_TypeC )
	  { return 'c'; }
      }
    return '.';
  };
  void CellInit( Cell &INPUT, int &RANDINT )
  { /* Initialization of the board happens at the
     * cell-level. Based on a randomized input I
     * iterate over the board and toggle flags. */
    switch( RANDINT )
      {
      case 1:
      case 2:
	{ INPUT.bit_ON( BIT_Resource | BIT_TypeA );
	  break;}
      case 3:
      case 4:
      	{ INPUT.bit_ON( BIT_Resource | BIT_TypeB );
      	  break; }
      case 5:
      	{ INPUT.bit_ON( BIT_Resource | BIT_TypeC );
      	  break; }
      default:
	{ INPUT.bitField_OFF();
	  break; }
      }
  };
  void GridOutput()
  { /* Main output loop for the grid */
    LoopPreY();
    for ( int Y = 0; Y < GridSize_Y; Y ++ ) {
	LoopPreX();
	for ( int X = 0; X < GridSize_X; X ++ )
	  {
	    LoopMain( ptr_activeGrid->Output( Y, X ) );
	  }
	LoopPosX(); }
    LoopPosY();
  };
  void GridInit()
  { /* Place initial bits randomly across the grid. */
    srand ((unsigned int) time (NULL) );  
    for ( int Y = 0; Y < GridSize_Y; Y ++ ) {
	for ( int X = 0; X < GridSize_X; X ++ )  {
	    int randInt = ( rand() % Grid_Rand );
	    CellInit( ptr_activeGrid->Output( Y, X ), randInt ); } }
  };
  void ExpandGridInit ()
  { /* Expand on bits, making them small patches instead of single units. */
    for ( int Y = 1; Y < GridSize_Y - 1; Y ++ ) {
	for ( int X = 1; X < GridSize_X - 1; X ++ ) {
	    ExpandCellInit( Y, X, ptr_activeGrid ->Output( Y, X ).Output()); } }
  };
  void ExpandCellInit( int Y, int X, unsigned int BIT )
  { /* Iterate over GridSize - 1 to avoid segfaulting when expanding. */
    for ( int y = (Y - 1); y < (Y + 1); y++) {
	for ( int x = (X - 1); x < (X + 1); x++)  {
	    ptr_activeGrid->Output( y, x ).bit_ON( BIT ); } }
  }
  void PlayerPlace()
  { /* Updates the position of the player on the board by toggling OFF the last
     * position and toggling ON the current position. */
    ptr_activeGrid->Output2( ptr_activePlayer->getLstPos() ).bit_OFF( BIT_Player );
    ptr_activeGrid->Output2( ptr_activePlayer->getCurPos() ).bit_ON ( BIT_Player );
  };
  void PlayerInput()
  { /* This checks whether or not the player picked up a resource. If it did,
     * we just toggle off resources at the position of the player. */
    Cell *tmpCell = &ptr_activeGrid->Output2( ptr_activePlayer->getCurPos() );
    bool resc = ptr_activePlayer->PlayerInput( tmpCell->Output() );
    if ( resc ) {
      tmpCell->bit_OFF( BIT_Resource | BIT_TypeA );
      tmpCell->bit_OFF( BIT_Resource | BIT_TypeB );
      tmpCell->bit_OFF( BIT_Resource | BIT_TypeC ); }
    if ( tmpCell->Output() & BIT_Upgrade1 )
      {	GameStateIncrement(); }
  };
  void PlayerAtMarket()
  { /* Sorry for putting several functionCalls  on the same line here. This is
     * to compact the amount of lines used for padding and outputting a line of text..
     * Ideally all of this would be sent to the same outputbuffer which would wrap it
     * inside my [###] -wrappers, but oh-well... */
    LoopPreY();    LoopPreX();  printf("%*s",  BufferWidth, "" );   LoopPosX();    LoopPreX();
    printf("%*s",  BufferWidth, "You are now at the market. Q to exit. " );
    LoopPosX();    LoopPreX();  printf("%*s",  BufferWidth, "" );    LoopPosX();    LoopPreX();
    printf("%*s",  BufferWidth, "t to transfer materials to base. T to transfer to the Player. " );
    LoopPosX();   LoopPreX();
    printf("%*s",  BufferWidth, "M to purchase from the travelling Merchant.[ NOT IMPLEMENTED ] " );
    LoopPosX();    LoopPosY();
    PrintMessageBuffer();
    PrintMenuBuffer();
  };
  void EvalMarketInput( char &INPUT )
  {
    if ( INPUT == ('q'|'Q') )
      {	GameStateDecrement(); }
    if ( INPUT == 't' ) {
	Buildings[0]->Inv + ptr_activePlayer->getInv();
	ptr_activePlayer->getInv().Init( 0, 0, 0 ); }
    if ( INPUT =='T' ) {
	ptr_activePlayer->getInv() + Buildings[0]->Inv;
	Buildings[0]->Inv.Init( 0, 0, 0 ); }
  };
  void WorkerPlace()
  { /* All buildings have an AI. We iterate over the buildings to prompt their respective
     * AI's to move given an offset based in their index. If not given this offset, all
     * the AI's would move exactly the same. All buildings except for the base. */
    for ( int i = 1; i < Buildings.size(); i ++)
      {
	ptr_activeGrid->Output2( Buildings[i]->Worker.getLstPos() ).bit_OFF( BIT_Worker );
	ptr_activeGrid->Output2( Buildings[i]->Worker.getCurPos() ).bit_ON ( BIT_Worker );
	if ( Buildings[i]->Worker.CheckToggle() )
	  { /* This makes the Building worker output a resource of the
	     * type of building it comes from every tenth turn. */
	    ptr_activeGrid->Output2( Buildings[i]->Worker.getCurPos() ).bit_ON
	      ( (Buildings[i]->Type) ^ BIT_Building | BIT_Resource ); }
	Buildings[i]->Worker.MovePrompt( i );
      }
  };
  void vectorMath()
  { /* Helperfunction to check if I'd be able to implement vectors for the AI's.
     * At this point, with the logic in place, it shouldn't really be that hard,
     * but I'm kinda running out of time here. 
     * This function prints the unitvector of the user relative to its Base. */
    if (Buildings.size() >= 1) {
	Coordinates bPos = Buildings[0]->Pos;
	Coordinates aPos = ptr_activePlayer->getCurPos();
	bPos.Y -= aPos.Y;
	bPos.X -= aPos.X;
	printf("unitVector to base: %d %d", bPos.X, bPos.Y); }
  };
  
public:
  void Init()
  { /* We create the grid, expand it, place the player, and descend into the main gameloop. */
    GridInit();
    ExpandGridInit();
    PlayerPlace();
    GameStateIncrement();
  };
  void Output()
  { /* All ouputOperations of the board goes here. */
    ClearConsole();
    PrintPlayerStatsBuffer();
    PrintBaseStatsBuffer();
    GridOutput();
    PrintMessageBuffer();
    PrintMenuBuffer();
    // vectorMath();
  };
  void Input()
  { /* All input. */
    GetInput( INPUT );
    EvalInput( INPUT );
  };
  void Sync()
  { /* Synchronize the player and the grid. */
    WorkerPlace();
    PlayerPlace();
    PlayerInput();
    GameRoundIncrement();
  };
  void Market()
  { /* REMEMBER TO ASK FOR INPUT TO BE EVALUATED AT THE END OF A
     * LOOP TO AVOID THE BEAUTIFUL INIFINITE LOOP.
     * THANK YOU. */
    ClearConsole();
    PrintPlayerStatsBuffer();
    PrintBaseStatsBuffer();
    PrintMerchantBuffer();
    PlayerAtMarket();
    GetInput( INPUT );
    EvalMarketInput( INPUT );
  };
  Controller( Grid &activeGrid, Player &activePlayer, unsigned char &gameState )
  {
    ptr_activeGrid = &activeGrid;
    ptr_activePlayer = &activePlayer;
    ptr_gameState = &gameState;
    MessageBuffer = "We need a place to sleep tonight. Where are we going to sleep.. help..";
    StatsBuffer = ""; /* My attempt at creating a captivating storyline. */
    MenuBuffer = "";
    MerchantInv.Init(10, 10, 10);

    PlayerMoveString = "wsadWSAD";
    PlayerBuildString = "bB123";
    PlayerUpgradeString = "eE456";

    // Win32console = GetStdHandle(STD_OUTPUT_HANDLE);
    
    BufferWidth = (GridSize_X * 2);
    BufferW_Quad = (BufferWidth / 4);
    Grid_Pad_X = ptr_activeGrid->PadX();
    Grid_Pad_Y = ptr_activeGrid->PadY();
    Grid_Rand = 80;
    GameRound = 0;
    INPUT = 0;
  };
  ~Controller()
  { /* Turns out std::vector had it's own destructor, so I'm doing the nasty shortcut
     * of using its own destructor instead of writing my own. IF I wrote my own, though, 
     * I would probably iterate over std::vector.size(), and call std::vector.erase( i ). */
    Buildings.~vector();
  };
};
#endif
