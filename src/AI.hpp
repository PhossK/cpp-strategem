#ifndef STRATEGEM_AI
#define STRATEGEM_AI
#pragma once

struct AI
{ /*
   * Very similar to the PlayerStruct, and both of these should
   * ideally inherit from the same base-class that holds all
   * the common attributes, but I wasn't that clever.
   * This AI moves about until it finds a resource, and
   * then returns back to its startingposition.
   */
  int Age;
  unsigned int Type;
  Inventory Inv;
  Coordinates BasePos;
  Coordinates CurPos;
  Coordinates LstPos;
  Coordinates UnitVector;

  void WorkerCheckPos()
  {
    /*
     * Make sure we don't step out of bounds!
     * Pos < Y; Pos > Y; Pos < X; Pos > X;
     */
    if ( CurPos.Y < 0 )
      {	CurPos.Y = 0;      }
    if ( CurPos.Y > GridSize_Y - 1 )
      {	CurPos.Y = GridSize_Y - 1;      }
    if ( CurPos.X < 0 )
      {	CurPos.X = 0;      }
    if ( CurPos.X > GridSize_X - 1 )
      {	CurPos.X = GridSize_X - 1;      }
  };
  Coordinates getVector()
  {
    UnitVector.Y -= CurPos.Y;
    UnitVector.X -= CurPos.X;
    return UnitVector;
  };
  int RandDir( int RandOffset )
  {
    srand ((unsigned int) time (NULL) );
    return ((rand() + RandOffset) % 4);
  };
  bool CheckToggle()
  {
    if (Age % 10 == 0)
      {
	return true;
      }
    return false;
  };
  void WorkerMovePos( int direction )
  {
    switch ( direction ) {
    case 0: {
      CurPos.Y -= 1;
      break; 
    }
    case 1: {
      CurPos.Y += 1;
      break; 
    }
    case 2: {
      CurPos.X -= 1;
      break; 
    }
    case 3: {
      CurPos.X += 1;
      break; 
    }
    default:
      break; 
    }
  };
  bool WorkerInput( unsigned int &INPUT )
  {
    if ( INPUT & BIT_Resource )
      {
     	if ( INPUT & BIT_TypeA )
	  {
	    Inv.ResourceA ++;
	    return true;
	  }
	else if ( INPUT & BIT_TypeB )
	  {
	    Inv.ResourceB ++;
	    return true;
	  }
	else if ( INPUT & BIT_TypeC )
	  {
	    Inv.ResourceC ++;
	    return true;
	  }
      }
    return false;
  };
  void SyncLstPos()
  {
    LstPos = CurPos;
  };
  Coordinates getBasePos()
  {
    return BasePos;
  };
  Coordinates getCurPos()
  {
    return CurPos;
  };
  Coordinates getLstPos()
  {
    return LstPos;
  };
  Inventory &getInv()
  {
    return Inv;
  };
  void InitAIPos( Coordinates &newBasePos )
  {
    BasePos = newBasePos;
    CurPos = BasePos;
    UnitVector = BasePos;
    SyncLstPos();
  };
  void MovePrompt( int RandOffset )
  {
    SyncLstPos();
    WorkerMovePos( RandDir( RandOffset ) );
    WorkerCheckPos();
    Age ++;
  };
  AI()
  {
    //
  };
  ~AI()
  {
    //
  };
  
};

#endif
