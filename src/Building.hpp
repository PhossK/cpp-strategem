#ifndef STRATEGEM_BUILDING
#define STRATEGEM_BUILDING
#pragma once

struct Building
{
  unsigned int Type;
  Coordinates Pos;
  Inventory Inv;
  AI Worker;
};

#endif
