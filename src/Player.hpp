#ifndef STRATEGEM_PLAYER
#define STRATEGEM_PLAYER
#pragma once

struct Player
{
private:
  Coordinates CurPos;
  Coordinates LstPos;
  Inventory Inv;
  
  void InitCurPos()
  {
    CurPos.Y = (GridSize_Y / 2);
    CurPos.X = (GridSize_X / 2);
  };
  void SyncLstPos()
  {
    LstPos = CurPos;
  };
  void PlayerMove( char &INPUT )
  {
    switch ( INPUT )
      {
      case 'w':
      case 'W':
	{
	  CurPos.Y -= 1;
	  break;
	}
      case 's':
      case 'S':
	{
	  CurPos.Y += 1;
	  break;
	}
      case 'a':
      case 'A':
	{
	  CurPos.X -= 1;
	  break;
	}
      case 'd':
      case 'D':
	{
	  CurPos.X += 1;
	  break;
	}
      default:
	{
	  break;
	}
      };
  };
  void PlayerCheckPos()
  {
    /*
     * Make sure we don't step out of bounds!
     * Pos < Y; Pos > Y; Pos < X; Pos > X;
     */
    if ( CurPos.Y < 0 )
      {	CurPos.Y = 0;      }
    if ( CurPos.Y > GridSize_Y - 1 )
      {	CurPos.Y = GridSize_Y - 1;      }
    if ( CurPos.X < 0 )
      {	CurPos.X = 0;      }
    if ( CurPos.X > GridSize_X - 1 )
      {	CurPos.X = GridSize_X - 1;      }
  };

public:
  Coordinates getCurPos()
  { return CurPos; };
  Coordinates getLstPos()
  { return LstPos; };
  Inventory &getInv()
  { return Inv; };
  bool PlayerInput( unsigned int &INPUT )
  {
    if ( INPUT & BIT_Resource )
      {
     	if ( INPUT & BIT_TypeA )
	  {
	    Inv.ResourceA ++;
	    return true;
	  }
	else if ( INPUT & BIT_TypeB )
	  {
	    Inv.ResourceB ++;
	    return true;
	  }
	else if ( INPUT & BIT_TypeC )
	  {
	    Inv.ResourceC ++;
	    return true;
	  }
      }
    return false;
  };
  bool CanAfford( Inventory &COST )
  {
    bool A = ( Inv.ResourceA >= COST.ResourceA );
    bool B = ( Inv.ResourceB >= COST.ResourceB );
    bool C = ( Inv.ResourceC >= COST.ResourceC );
    if ( A && B && C )
      {
	Inv - COST;
	return true;
      }
    else
      {
	return false;
      };
  };    
  void PlayerMovePrompt( char &INPUT )
  {
    SyncLstPos();
    PlayerMove( INPUT );
    PlayerCheckPos();
  };
  Player()
  {
    InitCurPos();
    SyncLstPos();
  };
  ~Player()
  {
    //
  };
  
};

#endif
